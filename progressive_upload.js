$(document).ready(async function () {
  // DAM production host
  const DAM_PROD_HOST = "https://apidam.jixie.io";
  // DAM RC host
  const DAM_RC_HOST = "https://jx-dam-api-express-rc.azurewebsites.net";
  // DAM Localhost 
  const DAM_LOCALHOST = "http://localhost:3000";
  // Chunk file size in MB
  const CHUNK_SIZE_IN_MB = 10;
  const fileInput = document.getElementById("video-file");
  const submitForm = $("#submitForm");
  const envSelect = $("#env-select");
  let damHost = DAM_RC_HOST;
  let videoId, collectionId, accessKey;
  const resultsDiv = $("#results");
  const collectionInput = $("#collection_id");

  // Select environment PROD | RC to upload
  envSelect.on("change", (e) => {
    const selectOption = envSelect.val();
    if (selectOption === "rc") {
      damHost = DAM_RC_HOST;
    } else if (selectOption === "prod") {
      damHost = DAM_PROD_HOST;
    }else if(selectOption == "local"){
      damHost = DAM_LOCALHOST; 
    }
  });

  submitForm.on("submit", async function (e) {
    e.preventDefault();
    if (fileInput.files.length === 0) {
      alert("Please select a file");
      return;
    }
    videoId = $("#videoId").val();
    accessKey = $("#accessKey").val();
    collectionId = collectionInput.val();
    console.log("collectionId", collectionId);
    const file = fileInput.files[0];
    console.log("file", file);
    if (!videoId) {
      videoId = await createVideoInColletion(file.name, collectionId);
    }
    if (!accessKey) return alert("Missing accessKey");
    const uploadId = await getUploadId(videoId);
    await cutAndUploadVideo(file, videoId, uploadId);
  });

  /**
   * @desc Initiate progressive upload to get the upload id
   * @param {number|string} videoId
   * @returns {Promise<*>}
   */
  async function getUploadId(videoId) {
    const initChunkUploadUrl = `${damHost}/api/videos/${videoId}/upload-chunk-init`;
    const requestBody = {
      filename: fileInput.files[0].name,
    };
    return await fetch(initChunkUploadUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        JIXIE_ACCESS_KEY: accessKey,
      },
      body: JSON.stringify(requestBody),
    })
      .then((res) => res.json())
      .then((data) => data.data.upload_id)
      .catch((e) => console.log("[getUploadId]", e));
  }

  /**
   * @desc Cut the video into smaller chunks and upload all chunks simultaneously
   * @param file - The video file
   * @param videoId
   * @param uploadId - Upload ID received after initialized progressive upload
   * @returns {Promise<void>}
   */
  async function cutAndUploadVideo(file, videoId, uploadId) {
    // Chunk size in byte
    const CHUNK_SIZE_IN_BYTE = CHUNK_SIZE_IN_MB * Math.pow(10, 6);
    const CHUNK_COUNT = Math.ceil(file.size / CHUNK_SIZE_IN_BYTE);
    const promises = [],
      parts = [];
    let fromByte = 0,
      toByte = CHUNK_SIZE_IN_BYTE,
      count = 0;

    // Append upload progress to UI
    resultsDiv.append(
      `<p class="text-primary fw-bold">RANDOM VIDEO ID: ${videoId}</p>`
    );
    resultsDiv.append(
      `<p class="text-primary fw-bold">CHUNK SIZE - TOTAL VIDEO SIZE: ${CHUNK_SIZE_IN_BYTE} bytes - ${file.size} bytes</p>`
    );
    resultsDiv.append(
      `<p class="text-primary fw-bold">TOTAL CHUNKS: ${CHUNK_COUNT}</p>`
    );
    resultsDiv.append(
      `<p class="text-primary fw-bold">UPLOADING CHUNKS TO SERVER: ${Math.ceil(
        file.size / CHUNK_SIZE_IN_BYTE
      )}</p>`
    );

    while (fromByte < file.size) {
      promises.push(
        new Promise((resolve, reject) => {
          count += 1;
          resultsDiv.append(`<p>Starting uploading Chunk ${count}</p>`);
          try {
            // Cut the video to smaller chunk files
            const chunkFile = file.slice(fromByte, toByte);
            // Upload the chunk file
            const result = uploadChunk(
              file.name,
              chunkFile,
              videoId,
              uploadId,
              { fromByte, toByte, totalBytes: file.size }
            );
            resolve(result);
          } catch (e) {
            reject(e);
          }
          fromByte = toByte;
          toByte = fromByte + CHUNK_SIZE_IN_BYTE;
          toByte = toByte > file.size ? file.size : toByte;
        })
      );
    }
    // Upload all chunks simultaneously
    await Promise.all(promises)
      .then(async (result) => {
        for (let data of result) {
          // Keep the chunk etag after uploaded
          parts.push({
            chunk_number: data.data.chunk_number,
            etag: data.data.etag,
          });
        }
        resultsDiv.append(
          `<p class="text-success">All chunks uploaded, merging all chunks into one file</p>`
        );

        // After all chunks uploaded, merge all chunks into one file
        await mergeChunks(file.name, videoId, uploadId, parts);
      })
      .catch((e) => {
        console.log("[cutAndUploadVideo]", e);
      });
  }

  /**
   * @desc Upload a single chunk
   * @param {string} filename - Name of the video file
   * @param file - chunk file after sliced from original video file
   * @param {number} videoId
   * @param {string} uploadId - Upload ID received after initialized progressive upload
   * @param {number} fromByte - The start byte of the chunk file
   * @param {number} toByte - The end byte of the chunk file
   * @param {number} totalBytes - Total size of the video file in byte
   * @returns {Promise<{success}|any>}
   */
  async function uploadChunk(
    filename,
    file,
    videoId,
    uploadId,
    { fromByte, toByte, totalBytes }
  ) {
    const uploadChunkUrl = `${damHost}/api/videos/${videoId}/upload-chunk/${uploadId}`;
    const formData = new FormData();
    formData.append("chunkFile", file);
    formData.append("filename", filename);
    const headers = {
      JIXIE_ACCESS_KEY: accessKey,
      "Content-Range": `bytes ${fromByte}-${toByte}/${totalBytes}`,
    };
    return await fetch(uploadChunkUrl, {
      method: "POST",
      body: formData,
      headers,
    })
      .then((res) => res.json())
      .catch((e) => console.log("[uploadChunk]", e));
  }

  /**
   * @desc Merge all chunks into one file
   * @param filename
   * @param videoId
   * @param uploadId - Upload ID received after initialized progressive upload
   * @param chunks
   * @returns {Promise<*>}
   */
  async function mergeChunks(filename, videoId, uploadId, chunks) {
    const mergeChunksUrl = `${damHost}/api/videos/${videoId}/combine-chunks/${uploadId}`;
    const headers = {
      "Content-Type": "application/json",
      JIXIE_ACCESS_KEY: accessKey,
    };
    const requestPayload = {
      filename,
      chunks,
    };
    return await fetch(mergeChunksUrl, {
      method: "POST",
      headers,
      body: JSON.stringify(requestPayload),
    })
      .then((data) => data.json())
      .catch((e) => console.log("[mergeChunks]", e));
  }

  /**
   * @desc Create new video record to retrieve the video's id
   * @param filename - The video file name
   * @param collectionId - video's collection id
   * @returns {Promise<*>}
   */
  async function createVideoInColletion(filename, collectionId) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("JIXIE_ACCESS_KEY", accessKey);

    const requestBody = JSON.stringify({
      title: filename,
    });

    const requestOptions = {
      method: "POST",
      headers,
      body: requestBody,
      redirect: "follow",
    };

    const result = await fetch(
      `${damHost}/api/collections/${collectionId}/videos/create`,
      requestOptions
    );
    const jsonData = await result.json();
    if (jsonData.success) return jsonData.data.id;
    throw jsonData;
  }
});
